/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.ArrayList;

/**
 *
 * @author owen_
 */
public class Handler {
 ArrayList<GameObject> objects;

    public Handler() {
        objects = new ArrayList<GameObject>();
    }
 
    public void addObject(GameObject object){
        objects.add(object);
    }
    public void removeObject(GameObject object){
        objects.remove(object);
        object = null;
    }
    public void render(Graphics2D g){
        for (GameObject object : objects) {
            
                    object.render(g);
        }
    }
    
    public void tick(){
        for (int i = 0; i < objects.size(); i++) {
            
            GameObject object = objects.get(i);
 
            object.tick();
            for (int j = 0; j < objects.size(); j++) {
                GameObject object2 = objects.get(j);
                if(object2.bounds.all.intersects(object.bounds.all) && (object.id == ID.frec || object.id == ID.incomingFirstYear))
                    object.collision(object2);
                
            }
            object.move();
            
             
        }
    }
}
