
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author owen_
 */
public class Frec extends GameObject{

    boolean onGround = false, pointingR = true;
    Animation frecAniRight,frecAniLeft;
    public Frec() {
        super(200, 400, 64, 128, ID.frec);
        BufferedImage[] imgsFor = {ImageLoader.imageLoader("graphics/WalkingFRECSprite1.png"), ImageLoader.imageLoader("graphics/WalkingFRECSprite2.png"),ImageLoader.imageLoader("graphics/WalkingFRECSprite3.png"),ImageLoader.imageLoader("graphics/WalkingFRECSprite4.png"),ImageLoader.imageLoader("graphics/WalkingFRECSprite5.png"),ImageLoader.imageLoader("graphics/WalkingFRECSprite6.png"),ImageLoader.imageLoader("graphics/WalkingFRECSprite7.png"),ImageLoader.imageLoader("graphics/WalkingFRECSprite8.png")};
        BufferedImage[] imgsBac = {ImageLoader.imageLoader("graphics/WalkingFRECSprite1l.png"), ImageLoader.imageLoader("graphics/WalkingFRECSprite2l.png"),ImageLoader.imageLoader("graphics/WalkingFRECSprite3l.png"),ImageLoader.imageLoader("graphics/WalkingFRECSprite4l.png"),ImageLoader.imageLoader("graphics/WalkingFRECSprite5l.png"),ImageLoader.imageLoader("graphics/WalkingFRECSprite6l.png"),ImageLoader.imageLoader("graphics/WalkingFRECSprite7l.png"),ImageLoader.imageLoader("graphics/WalkingFRECSprite8l.png")};

        frecAniRight = new Animation(imgsFor, 8, true);
        frecAniLeft = new Animation(imgsBac, 9, true);
    }

    @Override
    public void render(Graphics2D g) {
       // g.draw(bounds.all);
        if(pointingR)
            frecAniRight.render(g, xPosition, yPosition, bounds.all.width, bounds.all.height);
        else
            frecAniLeft.render(g, xPosition, yPosition, bounds.all.width, bounds.all.height);
        
    }

    @Override
    public void tick() {
        super.tick(); 
        
       frecAniRight.tick();
       frecAniLeft.tick();
        if(Key.up.isDown && onGround){
            yVelocity = -5;
            if(aliveForTicks % 112  < 20 || aliveForTicks % 112 >102){
                Game.points += 10;
                if (aliveForTicks % 112  < 10)
                    Game.points += 30;
            }
        }else if(onGround)
            yVelocity = 0;
        if(Key.left.isDown){
            xVelocity = -4;
            pointingR = false;
        }
        if(Key.right.isDown){
            xVelocity = 4; 
            pointingR = true;
        }
           
        if(!onGround)
            yVelocity += 0.16;
        
        if(xPosition <= 0){
            xVelocity = 4;
            pointingR = true;
        }
            
        if(xPosition >=700){
            xVelocity = -4;
            pointingR = false;
        }
        onGround = false;
        
    }

    @Override
    public void collision(GameObject gO) {
         if(gO.id == ID.floor)
             onGround = true;
         if(gO.id == ID.incomingFirstYear && Key.help.isDown){
             if(((IncomingFirstYear)gO).fallenDown)
                ((IncomingFirstYear)gO).standUp();
         }
    }
    
}
