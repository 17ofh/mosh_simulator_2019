
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import java.util.HashMap;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;


public class Game extends JPanel implements KeyListener, Runnable{

    public HashMap<Integer, Key> keyBindings = new HashMap<>();  
   public boolean running = false;
   public static Handler handler;
   Thread th;
   public static Game game;
    public JFrame frame;
    public static int unsafeIFYs = 0, points = 0;
     JLabel unsafeIFYLabel, pointsLabel;
    
    public Game(){
        
     frame = new JFrame("Orientation Week Simulator 2019");
     frame.setVisible(true);
     frame.setSize(800,800);
     frame.setLocationRelativeTo(null);
     frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
     frame.addKeyListener(this);
     frame.add(this);
     frame.setResizable(false);
     start();
     
    }
    
      private synchronized void start() {
        // If the program is already running then do nothing but if not running,
        // make it run and start the thread
        if (running) {
            return;
        }
        running = true;
        th = new Thread(this);
        th.start();
    }
      
      private synchronized void stop() {
        if (!running) {
            return;
        }
        running = false;
        try {
            th.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.exit(1);
    }
      
      public void run() {
        init();
        long lastTime = System.nanoTime();
        final double numberOfTicks = 60.0;
        double ns = 1000000000 / numberOfTicks;
        double delta = 0;
        int updates = 0;
        int frames = 0;
        long timer = System.currentTimeMillis();

        while (running) {
            long now = System.nanoTime();
            delta += (now - lastTime) / ns;
            lastTime = now;
            if (delta >= 1) {
                tick();
                delta--;
                updates++;
            }

            frames++;

            if (System.currentTimeMillis() - timer > 1000) {
                timer += 1000;
                System.out.println("Ticks: " + updates
                        + "      Frames Per Second(FPS): " + frames);
                
                updates = 0;
                frames = 0;
            }
        }
    }
      
       private void tick() {
        
        repaint();
        handler.tick();
       
        unsafeIFYLabel.setText("Number of Unsafe First Years: " + unsafeIFYs);
        pointsLabel.setText("Points: " + points);
        if(unsafeIFYs > 10){
            GameOver g = new GameOver(frame, true, points);
            g.setVisible(true);
        }
    }
       
       
   @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
     
        render(g);
        
    }

    private void render(Graphics g) {
        Graphics2D g2d = (Graphics2D) g;
        
        handler.render(g2d);
       
    }
    
    
    private void init(){
        bind(KeyEvent.VK_UP, Key.up);
        bind(KeyEvent.VK_DOWN, Key.down);
        bind(KeyEvent.VK_LEFT, Key.left);
        bind(KeyEvent.VK_RIGHT, Key.right);
        bind(KeyEvent.VK_SPACE, Key.help);
        handler = new Handler();
        
        pointsLabel = new JLabel();
        pointsLabel.setLocation(0, 250);
        pointsLabel.setSize(128, 256);
        pointsLabel.setFont(new Font("Times New Roman", Font.BOLD, 50));
        unsafeIFYLabel = new JLabel("Number of Unsafe First Years: 0");
        unsafeIFYLabel.setLocation(0 , 0);
        unsafeIFYLabel.setSize(128, 256);
        unsafeIFYLabel.setFont(new Font("Times New Roman", Font.BOLD, 50));
        add(unsafeIFYLabel);
        add(pointsLabel);
        
        handler.addObject(new Floor());
        handler.addObject(new Frec());
        
        handler.addObject(new IncomingFirstYear(100, 64, 128));
       
        handler.addObject(new IncomingFirstYear(200, 64, 128));
        handler.addObject(new IncomingFirstYear(300, 64, 128));
        handler.addObject(new IncomingFirstYear(400, 64, 128));
        handler.addObject(new IncomingFirstYear(500, 64, 128));
        handler.addObject(new IncomingFirstYear(600, 64, 128));
        
        try{
        SoundManager.playMusic();
        }catch(Exception e){
            e.printStackTrace();
        }
        IntroScreen screen = new IntroScreen(frame, true);
        screen.setVisible(true);
        screen = null;
    }
     
     
    
    public static void main(String[] args) {
        game = new Game();
    }
    
     @Override
    public void keyTyped(KeyEvent e) {
      
    }

    @Override
    public void keyPressed(KeyEvent e) {
         try {
            keyBindings.get(e.getKeyCode()).isDown = true;
        } catch (Exception ep) {

        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
         try {
            keyBindings.get(e.getKeyCode()).isDown = false;
        } catch (Exception ep) {

        }
    }
    
    public void bind(Integer keyCode, Key key) {
        keyBindings.put(keyCode, key);
    }
     
    
}
