/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.awt.Graphics;
import java.awt.Graphics2D;



/**
 *
 * @author owen_
 */
public abstract class GameObject {
    double xPosition, yPosition, xVelocity, yVelocity;
    Bounds bounds;
    ID id;
    int aliveForTicks = 0;
    boolean canKill = false;
    public GameObject(double xPosition, double yPosition, double xVelocity, double yVelocity, double width, double height, ID id) {
        this.xPosition = xPosition;
        this.yPosition = yPosition;
        this.xVelocity = xVelocity;
        this.yVelocity = yVelocity;
       bounds = new Bounds(xPosition, yPosition,width, height);
       this.id = id;
    }
    
    public GameObject(double xPosition, double yPosition,  double width, double height, ID id) {
        this.xPosition = xPosition;
        this.yPosition = yPosition;
        this.xVelocity = 0;
        this.yVelocity = 0;
       bounds = new Bounds(xPosition, yPosition,width, height);
       this.id = id;
    }
    
    public abstract void render(Graphics2D g);
    
    public void tick (){
     aliveForTicks++;
    }

    public double getxPosition() {
        return xPosition;
    }

    public void setxPosition(double xPosition) {
        this.xPosition = xPosition;
    }

    public double getyPosition() {
        return yPosition;
    }

    public void setyPosition(double yPosition) {
        this.yPosition = yPosition;
    }

    public double getxVelocity() {
        return xVelocity;
    }

    public void setxVelocity(double xVelocity) {
        this.xVelocity = xVelocity;
    }

    public double getyVelocity() {
        return yVelocity;
    }

    public void setyVelocity(double yVelocity) {
        this.yVelocity = yVelocity;
    }

    public Bounds getBounds() {
        return bounds;
    }

    public void setBounds(Bounds bounds) {
        this.bounds = bounds;
    }
    
    public void collision(GameObject gO){
        
    }
    
    public void move(){
     xPosition += xVelocity;
     yPosition += yVelocity;
     bounds.update(xPosition, yPosition);   
    }
    
}
