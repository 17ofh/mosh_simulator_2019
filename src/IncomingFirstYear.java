
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;

public class IncomingFirstYear extends GameObject {

    public boolean fallenDown = false;
    boolean onGround = false;
    int fallenDownTicks = 0;
    int diff = 50;
    boolean pointingR = true;
    Animation iFYAniRight, iFYAniLeft;
    BufferedImage fallenImg;
    public IncomingFirstYear(double xPosition, double width, double height) {
        super(xPosition, 572, width, height, ID.incomingFirstYear);
        BufferedImage[] imgsR = {ImageLoader.imageLoader("graphics/IncomingFirstYear1.png"),ImageLoader.imageLoader("graphics/IncomingFirstYear2.png"),ImageLoader.imageLoader("graphics/IncomingFirstYear3.png"),ImageLoader.imageLoader("graphics/IncomingFirstYear4.png"),ImageLoader.imageLoader("graphics/IncomingFirstYear5.png"),ImageLoader.imageLoader("graphics/IncomingFirstYear6.png"),ImageLoader.imageLoader("graphics/IncomingFirstYear7.png"),ImageLoader.imageLoader("graphics/IncomingFirstYear8.png")};
        BufferedImage[] imgsL = {ImageLoader.imageLoader("graphics/IncomingFirstYear1l.png"),ImageLoader.imageLoader("graphics/IncomingFirstYear2l.png"),ImageLoader.imageLoader("graphics/IncomingFirstYear3l.png"),ImageLoader.imageLoader("graphics/IncomingFirstYear4l.png"),ImageLoader.imageLoader("graphics/IncomingFirstYear5l.png"),ImageLoader.imageLoader("graphics/IncomingFirstYear6l.png"),ImageLoader.imageLoader("graphics/IncomingFirstYear7l.png"),ImageLoader.imageLoader("graphics/IncomingFirstYear8l.png")};
        iFYAniRight = new Animation(imgsR, 8, true);
        iFYAniLeft = new Animation(imgsL, 8, true);
        fallenImg = ImageLoader.imageLoader("graphics/IncomingFirstYearDown.png");
    }

    @Override
    public void tick() {
        super.tick();
        if(pointingR)
            iFYAniRight.tick();
        else
            iFYAniLeft.tick();
        if (aliveForTicks % 70 == 5) {
            xVelocity = ((Math.random() * 1000) % 8) - 4;
        }
        if (xPosition > 700 || xPosition < 20) {
            xVelocity = -xVelocity;
        }

        if (aliveForTicks %112 <10 && onGround && !fallenDown) {
            yVelocity = -5;
        } else if (onGround) {
            yVelocity = 0;
        }
        if (!onGround) {
            yVelocity += 0.16;
        }
        if(Math.random() * 100000 < diff && !fallenDown){
            bounds.fallDown();
            fallenDown = true;
            
            
            
        }
        
        if(fallenDown && onGround)
            xVelocity = 0;
        if(fallenDown){
            fallenDownTicks ++;
        }else{
            fallenDownTicks = 0;
        }
        if(fallenDownTicks > 120){
            standUp();
            Game.unsafeIFYs ++;
        }
            
            
        if(aliveForTicks % 240 == 0)
            diff+=20;
        
        pointingR = (xVelocity > 0);
            
        onGround = false;

    }

    @Override
    public void render(Graphics2D g) {
        //g.draw(bounds.all);
        if(pointingR && !fallenDown){
            iFYAniRight.render(g, xPosition, yPosition, bounds.all.width, bounds.all.height);
            
        }else if(!pointingR && ! fallenDown){
             iFYAniLeft.render(g, xPosition, yPosition, bounds.all.width, bounds.all.height);
             
        }else{
            AffineTransform t = new AffineTransform();
            t.translate(xPosition, yPosition);
            t.scale(bounds.all.width/fallenImg.getWidth(), bounds.all.height/fallenImg.getHeight());
            Graphics2D g2D = g;
        
            g2D.drawImage(fallenImg, t, null); // Draw the image
        }
        
        
    }

    @Override
    public void collision(GameObject gO) {
        if (gO.id == ID.floor) {
            onGround = true;
        }
    }
    
    public void standUp(){
        bounds.standUp();
        yPosition = bounds.all.y;
        fallenDown = false;
    }

}
