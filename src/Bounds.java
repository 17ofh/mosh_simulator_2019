/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



import java.awt.geom.Rectangle2D;

/**
 *
 * @author owen_
 */
public class Bounds {
    
    Rectangle2D.Double top, bottom, left, right, all;
    double width, height, preFallX, preFallY;
    public Bounds (double x,double y, double width,double height){
        top = new Rectangle2D.Double(x,y, width, 2);
        bottom = new Rectangle2D.Double(x, y + height - 2, width, 2);
        left = new Rectangle2D.Double(x,y,2, height);
        right = new Rectangle2D.Double(x + width - 2, y, 2, height);
        all = new Rectangle2D.Double(x,y,width, height);
        this.width = width;
        this.height = height;
    }

    public Rectangle2D.Double getTop() {
        return top;
    }

    public Rectangle2D.Double getBottom() {
        return bottom;
    }

    public Rectangle2D.Double getLeft() {
        return left;
    }

    public Rectangle2D.Double getRight() {
        return right;
    }

    public Rectangle2D.Double getAll() {
        return all;
    }
    
    public void update(double x,double y){
        top.setRect(x , y, width, height);
        bottom.setRect(x, y + height - 2, width, 2);
        left.setRect(x ,y + 5, 2 , height - 10);
        right.setRect(x + width - 2, y + 5, 2 , height - 10);
        all.setRect(x, y, width, height);
       
    }
    
    public void fallDown(){
        preFallX = all.x;
        preFallY = all.y;
        double temp = width;
        width = height;
        height = temp;
        update(all.x, all.y + height);
    }
    
    public void standUp(){
        
        double temp = width;
        width = height;
        height = temp;
        update(preFallX, preFallY);
       
    }
    
    
}
