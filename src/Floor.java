
import java.awt.Graphics2D;


public class Floor extends GameObject{

    public Floor() {
        super(0, 700, 800, 200, ID.floor);
    }

    @Override
    public void render(Graphics2D g) {
        g.fill(bounds.all);
    }
    
}
