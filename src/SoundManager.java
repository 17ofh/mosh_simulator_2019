/*
 * Class allows entire project to easily play sounds
 * The file paths for sounds is contained here
 */
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

public class SoundManager {
    
    private static final String SOUNDS_LOCATION = "src/sounds/";
    

    
    protected static void playMusic() throws IOException, UnsupportedAudioFileException, LineUnavailableException, InterruptedException, URISyntaxException {
        playClip(SoundManager.class.getResource( "music.wav")); //change location
    }
    
    
    
    private static void playClip(URL file) {
        try {
            Clip clip = AudioSystem.getClip();
            clip.open(AudioSystem.getAudioInputStream(file));
            clip.start();
            clip.loop(50);
        } catch (IOException | LineUnavailableException | UnsupportedAudioFileException e) {
            System.out.println("Sound unable to be played");
        }
    }
}
